package ru.nsu.crackhashworker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CrackhashworkerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrackhashworkerApplication.class, args);
    }

}
