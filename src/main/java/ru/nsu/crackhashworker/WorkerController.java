package ru.nsu.crackhashworker;


import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.nsu.crackhashworker.model.CrackHashManagerRequest;
import ru.nsu.crackhashworker.service.WorkerBrain;


@RestController()
@RequestMapping()
@AllArgsConstructor
public class WorkerController {
    static final Logger log = LoggerFactory.getLogger(WorkerController.class);
    final WorkerBrain workerBrain;

    @PostMapping(value = "/internal/api/worker/hash/crack/task")
    public void task(@RequestBody CrackHashManagerRequest request) {

        log.info("Received request RequestId = " + request.getRequestId() + " PartCount = " + request.getPartCount() +
                " hash = " + request.getHash());

        workerBrain.calc(request.getRequestId(), request.getPartNumber(), request.getPartCount(), request.getHash(),
                request.getMaxLength(), request.getAlphabet());
    }
}
