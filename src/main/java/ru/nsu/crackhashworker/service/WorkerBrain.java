package ru.nsu.crackhashworker.service;

import org.paukov.combinatorics3.Generator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import ru.nsu.crackhashworker.model.CrackHashManagerRequest;
import ru.nsu.crackhashworker.model.CrackHashWorkerResponse;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

@Service
public class WorkerBrain {
    static final Logger log = LoggerFactory.getLogger(WorkerBrain.class);
    static final String resourceUrl = "http://localhost:8080/internal/api/manager/hash/crack/request";

    public void calc(String requestId, int part, int partCount, String hash, int length,
                            CrackHashManagerRequest.Alphabet alphabet){

//        localAlphabet = alphabet.getSymbols().subList(0, input.size()/2);

        Generator.permutation(alphabet.getSymbols())
                .withRepetitions(length)
                .stream()
                .forEach(System.out::println);

        CrackHashWorkerResponse.Answers answers = new CrackHashWorkerResponse.Answers();
        answers.getWords().add("one");
        answers.getWords().add("two");

        sendResponse(requestId, part, answers);
    }
    public void sendResponse(String requestId, int part, CrackHashWorkerResponse.Answers answers){
        RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

        try{

            CrackHashWorkerResponse response = new CrackHashWorkerResponse();

            response.setRequestId(requestId);
            response.setPartNumber(part);
            response.setAnswers(answers);

            HttpEntity<CrackHashWorkerResponse> request1 = new HttpEntity<>(response);
            log.info("response with RequestId = " + request1.getBody().getRequestId() + " formed");

            restTemplate.patchForObject(resourceUrl, request1, CrackHashWorkerResponse.class);

//            final URI url = new URI(resourceUrl);
//
//            ResponseEntity<CrackHashWorkerResponse> responseEntity = restTemplate.exchange(
//                    url, HttpMethod.PATCH, request1, CrackHashWorkerResponse.class);

            log.info("Sent PATCH request with RequestId = " + request1.getBody().getRequestId());
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }
    }
}
